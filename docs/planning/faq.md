#### Why am I not seeing a certain feature or behavior in the tool as described in this document?

As we deploy new versions of the application, your browser may still be using a cached copy of a
previous version, even upon a reload of the page.
You may need to force a complete reload by clearing your browser's cache.
Instructions for doing this may be found [here](https://www.wikihow.com/Clear-Your-Browser%27s-Cache).

#### How can I add new platforms so they are available for selection in the timeline?

It is planned that the ODSS will include an option for adding platforms. For the moment, please let Carlos or Kevin know.

#### What happens when 2 users try to modify the same token, as in changing a date range or description?

Short answer: Last save of the same pre-existing token "wins." New tokens, however, are all retained
(basically because each new token gets a unique internal ID.)

!!! warning
    Please note: the tool does not in general make any explicit control for concurrent modifications.
    So, please coordinate the use of the tool for editing purposes, in particular for modification of existing tokens.

#### What happened to the "Periods" option?

The "periods" handling was an interim strategy (only within the planning
editor) to organize the visualization of the timeline by periods of
interest while the official "view" element in the main ODSS application
was incorporated.

#### Do all period views point to the same timeline database?

- Yes. There is a single backend database supporting the information displayed in the editor.
- Worth noting that the period selected as you zoom in/out or drag your mouse, only has a visual
effect (that is, no changes at all in the database).
