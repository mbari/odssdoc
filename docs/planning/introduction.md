## ODSS Planning: What is it about?

- A central goal of the Planning component in ODSS is to support the overall planning of science
experiments/campaigns through an intuitive graphical interface where platforms can be assigned
deployments and missions within a spatiotemporal frame.

- Once a plan is defined (and iteratively refined during execution), it can be combined with other displayed
elements within the Situational Awareness tab.

- Conversely, for better context during planning, elements from Situation Awareness can be included in
the Planning tab.

- In general, the tool is intended to provide export/import capabilities to facilitate the exchange of
plan specifications with external tools. In particular, the export options can be a basis for refinement
and execution by platform teams. Additionally, refined plans can be imported so they are combined with
 other data elements in the ODSS.


## Conceptual model

![](img/ODSS-planning-elements.png)


### Tokens

The central element to compose the timeline in the editor is the "token."
A token represents a particular state, activity, or task to be associated with a particular platform on a particular time period.

...
