# ODSS Documentation source

**NOTE**: VERY PRELIMINARY

The master branch of this repository contains the source of the ODSS documentation.

The documentation source is written in plain text files using
[Markdown](http://daringfireball.net/projects/markdown/) syntax.
[MkDocs](http://www.mkdocs.org/) is used for the documentation site generation.

The documentation is automatically generated upon changes pushed to this repo,
with target location at http://docs.mbari.org/odss/

## Local testing

In a nutshell:

    $ mkdocs serve
    INFO    -  Building documentation...
    INFO    -  Cleaning site directory
    [I 160728 15:40:27 server:281] Serving on http://127.0.0.1:8000
    [I 160728 15:40:27 handlers:59] Start watching changes
    [I 160728 15:40:27 handlers:61] Start detecting changes

Then open http://127.0.0.1:8000 in your browser.

More details at the [MkDocs site](http://www.mkdocs.org/).
