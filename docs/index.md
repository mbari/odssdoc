![](img/odss-overview.png)

Welcome to the Oceanographic Decision Support System (ODSS).
The ODSS is a web application that provides a central location for accessing information
for realtime operations as well as post field experiment access to data.
This page will give you some helpful tips on using the ODSS.

## Tabs

![](img/odss-tabs.png)

The tabs across the top of the ODSS give you the ability to switch between the Situational Awareness and
Planning views and gives you access to Slack and STOQS. Clicking on the Situational Awareness or Planning tabs will
open those tools, respectively, in the frame below the tabs. If you click on the Collaboration tab, it will open
a new window and direct you to the MBARI Slack workspace. If you click on the STOQS tab, it will open a new window
pointing you to the Spatial Temporal Oceanographic Query System (STOQS).

### Situational Awareness Tab

This situational awareness tab is where you can monitor the progress of different platforms that are sending
their locations to the ODSS as well as access data layers and products that are assocatied with a particular
view in the ODSS.

![](img/odss-sa-toolbar.png)

![](img/odss-sa-toolbar-2.png)

The toolbar across the top of the situational awareness pane give you control over the view that you are
displaying in the ODSS as well as controlling the widow of time displayed in the map window. It also
has a couple of button to set and recall a location and zoom level on the map that you can return to.

The ODSS divides up different activities, or field experiments into logical groups that we call 'Views'.
A View will have a specific set of platforms, layers and data associated with it. It is a way to organize
things and to prevent being overwhelmed by all the the information contained in the ODSS. You select a
view by clicking on the drop-down box on the left side of the toolbar and choosing the view you want to
select.

![](img/odss-select-view.png)

By default, the ODSS starts in a "Live" mode which means that it uses the current date and time for the
current view. If left in "Live" mode, the map will update every minute to make sure the platforms that
are plotted on the map are showing their most recent locations. You can take it out of "Live" mode
by clicking on the backwards and forwards arrows to step through time.

![](img/odss-step-back-arrow.png)
![](img/odss-step-forward-arrow.png)

If you click on either of those arrows, the ODSS will switch out of "Live" mode into "Playback" mode as
can be seen in the toolbar

![](img/odss-playback.png)

As you click on the backwards and forwards arrows, it will increment the "Start" date time by the amount
of hours specified in the "Step(hrs)" box. You can change the size of the step either by entering a number
(including fractional numbers) into the "Step(hrs)" box or using the up and down arrows to adjust the
number.

![](img/odss-step-by.png)

The "Start" time window defines the most recent date and time of the data shown in the ODSS. For example,
it defines the latest location of all the platforms that are closest to that start date and time, but
before it. The tracks displayed on the map consist of platform locations ending with the Start date and extending
backwards in time that is defined by the "Plot(hrs)" input box. The plot input box basically defines how
long the platform track "tail" is. If you select a "Start" time of 2020-01-01T23:00:00 and choose a Plot value of
11 hours, you will get the platform track data from 2020-01-01T00:00:00 to 2020-01-01T23:00. You can either
input the "Plot(hrs)" number directly in the input box or adjust it with the up and down arrows. You can
also select a "Start" time by manually entering one or clicking on the calendar icon and choosing one.

![](img/odss-start-datetime.png)
![](img/odss-plot-length.png)

Once you are done with playback mode, you can simply click on the red circle with a line through it to the
right of the "Mode: Playback" and it will return you to "Live" mode.

![](img/odss-live.png)

The map will always remember where you last were looking when you leave the ODSS. When you return, it will
put you back to the most recent location and zoom level. As a help, you can define a "Home" location so that
you will have a convenient way to return to a known location if you get lost or it would take a lot of
panning to get back to your normal view. Once you have the view the way you want it and decide you want that
to be your "Home" view, click on the "Pin" icon on the right side of the toolbar and then you can return to
that location at any time simply by clicking on the "Home" icon next to it.


![](img/odss-home.png)
![](img/odss-pin.png)

### Data Pane

<table>
<tr>
<td style="padding: 10px;">
<img src="img/odss-data-pane-arrows.png" />
</td>
<td style="padding: 10px;">
<p>
    There is a slide-out pane on the left side of the ODSS. You can access the pane, by clicking on
    the
    double-arrow icon at the top of the pane and it will slide into view, moving the map to the
    right.
</p>
</td>
</tr>
</table>

### Platforms

<table style="1px solid black">
</tr>
<tr>
<td style="padding: 10px; width: 400px;">
<div><img src="img/odss-data-platforms.png" /></div>
</td>
<td style="padding: 10px;">
<p>
    The first pane that is shown in the Data pane is the platforms panel. It is a tree that shows
    all
    the platforms that have been linked to the view that was selected in the View drop down. When
    the boxes next to the platforms are checked, location data for that platform will be displayed
    on
    the map for the time window selected in the toolbar. You can individually add and remove
    platform
    tracks from the map by checking and unchecking the box next to each platform. If you want to
    check or uncheck all the platforms at once, you can use the check-box and box buttons at the top
    of the Platforms panel.
</p>
<div style="text-align: center;">
    <img src="img/odss-platforms-un-check-all.png" />
</div>
<p>
    The platforms that are displayed in each view are only a subset of the total list of platforms
    registered in the ODSS. You can add or remove platforms from the list displayed in the current
    view by clicking on the edit button at the top of the platforms panel.
</p>
<div style="text-align: center;">
    <img src="img/odss-panel-edit-icon.png" />
</div>
<p>
    This will open a window that will allow you to select or de-select platforms that you wanted
    linked to a view. When you are done with your selections, simply close the window by clicking
    on the 'x' in the upper right corner.
</p>
<div style="text-align: center;">
    <img src="img/odss-platform-selection-window.png" width="300" />
</div>
<p>
    You can zoom directly to a platforms track on the map by clicking on the name of the platform
    in the tree.
</p>
<div style="text-align: center;">
    <img src="img/odss-platform-selection.png" />
    <img src="img/odss-track-zoom.png" width="400" />
</div>
<p>
    You can also open a new window with a table of the last 5 locations for a specific platform by
    small track icon <img src="img/odss-track-data-icon.png" /> to the right of the
    platform name
</p>
<div style="text-align: center;">
    <img src="img/odss-track-data-window.png" width="400" />
</div>
</td>
</tr>
</table>

### Layers
<table border="1">
<tr>
<td style="padding: 10px; width: 400px;">
<div><img src="img/odss-data-layers.png" /></div>
</td>
<td style="padding: 10px;">
<p>
    The Layers panel allows you to turn on and off overlays of data or
    features on the map by checking or clearing the checkbox next to the
    layer name. Just as with the platforms pane, the layers pane only shows a subset
    of the layers that ODSS has registered. You can add and remove layers
    to/from the current view, but clicking on the edit icon at the top
    of the layer panel.
</p>
<div style="text-align: center;">
    <img src="img/odss-panel-edit-icon.png" />
</div>
<p>
    That will open a window that will allow you to add or remove layers
    from the the view. This method is a little different than the
    platforms window as you have to enter a name in the box to the
    right of the layer to both add it to the View and to define what
    folder the layer will be displayed in. If you clear the name of
    the folder from the box, it will remove it from the view. Note that
    if you type in name in the folder box, you have to click outside
    the box to get the name to persist. Once you are done, close the
    window by clicking on the 'x' in the upper right corner of the window.
</p>
<div style="text-align: center;">
    <img src="img/odss-layer-selection-window.png" width="400" />
</div>
<p>
    There is a feature that is under development that allows you to zoom
    to the extents of a particular layer by clicking on the layers name.
    The service that is serving the layer has to support a special request
    for this to happen and we are still working on way to improve things.
    You can always try clicking on the name of a layer to see if it will
    zoom somewhere. This is especially important for layers that have
    very small features.
</p>
</td>
</tr>
</table>

### Data

<table border="1">
<tr>
<td style="padding: 10px; width: 400px;">
<div><img src="img/odss-data-data.png" /></div>
</td>
<td style="padding: 10px;">
<p>
    The data pane allows you to browse the files that are in a repository
    that has been linked to the current view. You can drill down through
    the file tree by clicking on the '+' symbols next to folder to expand
    them. If there is a file that you are interested in downloading or
    opening in a new window, simply click on the file name. If it's a
    file that can be displayed by the browser, it will open it in a new
    window, if not it will be downloaded to your computer. If you suspect
    the files in the repository have changed, but you are not seeing the
    changes, click on the button at the top of the data panel with the
    small circle on it, that will refresh the data panel view.
</p>
</td>
</tr>
</table>

### Map

<table border="1">
<tr>
<td style="padding: 10px;">
<p>
    The map shows all tracks and data layers that are selected in
    the data pane that was covered in the previous section. In order
    to pan the map, simply click and hold on the map anywhere and
    then drag your mouse to move the map. You can zoom in and out of
    the map by either placing your cursor over the map and using your
    mouse wheel, or by using the zoom control buttons on the upper
    left corner of the map.
</p>
<div style="text-align: center;"><img src="img/odss-map-zoom-buttons.png" /></div>
<p>
    You can also zoom by holding the shift key down on your keyboard,
    then clicking and holding on the map and dragging to define a view
    box. Once you release the mouse, the map will zoom to best fit that
    box in the view.
</p>
<p>
    In the lower left corner is a set of controls that will show you
    the latitude and longitude of the mouse cursor as well as the
    depth of the water where the mouse cursor is located (you have
    to pause moving the mouse for the depth to show). In addition
    you will see a scale that gives you a sense of the size of features
    at the current zoom level of the map. It changes as you zoom in
    and out of the map.
</p>
<div style="text-align: center;"><img src="img/odss-lat-lon-scale.png" /></div>
<p>
    The map also has a control that allows you to draw lines on the map
    that provide you with distances and bearing of various tracks that you
    draw on the map. The control is located in the upper left corner of the map
    under the zoom controls
</p>
<div style="text-align: center;"><img src="img/odss-measure-controls.png" /></div>
<p>
    You start the measurement mode by clicking on the top button (arrow) in the
    measurement control (it will turn green). The green button means your cursor is
    in measure mode. You can simply starting clicking on the map to drop end points
    for lines. The control will show you both the bearing of the incoming path to the
    points as well as the bearing of the outgoing line (if there is one). It will also
    display the distance of the line leading up to the end point. The units of the
    line can be changed by clicking on the bottom button in the control and will show
    you which measurement units is currently on (meters, miles, or nautical miles).
    When you are done creating lines, you can click on the top button of the measurement
    control which will remove the green color and put the cursor back in regular
    navigation mode. If you want to remove the measurements, simply click on the 'x'
    button in the measurement controls.
</p>
<div style="text-align: center;"><img src="img/odss-measure-example-m.png"
        width="600" />
</div>
 </td>
</tr>
</table>
