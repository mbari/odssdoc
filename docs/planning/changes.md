**Recent noteworthy changes**

1.0.2 (2019-07-25)

- (Internal: Upgraded Quasar to 1.0.5, from 1.0.0.beta)

1.0.1b (2019-06-27)

- Removed the temporary hard-coded end date for "2019 Spring CANON" view.

1.0.0b (2019-05-30)

- Initial version of a re-implementation of the ODSS planning component
  based on a more modern and flexible framework, which will facilitate
  further improvements and better alignment with other UI developments.

    Note: The new version has been enabled today via the usual
    tab on the ODSS, alongside the previous version (in a different tab)
    while the new version is better tested and adjusted moving forward.
    Surely there will be adjustments needed to further improve the tool
    and fix bugs.  Also, documentation is still to be updated.
    Please get in touch if you have any questions.

    As a summary of the changes:

    - Added various buttons at the top of the timeline to perform various
      actions as an alternative to using gestures with the mouse or trackpad:
      zoom in, zoom out, center "today" date, zoom to all items in the selected view.
    - Improved editing capability for the geometries on the map.
    - Individual changes in the timeline items or when completing a
      concrete drawing/editing action on the map are immediately saved
      in the database. (General "Save" button has been removed.)
    - Improved ability to edit coordinates directly upon double-clicking layer on map.
    - Added ability to draw and edit circles.

    ![](img/ODSS_Planning_1.0.0b.png)


----

0.9.6 (2017-12-05)

- add estimated nominal speed for ahi, aku, and opah
- expand maximum visualization range
- internal: configuration simplification; upgrade several dependencies

0.9.4 (2017-04-06)

- make timeline section taller (~600px, previously 300px).
- put timeline axis also at the bottom

0.9.4 (2017-03-27)

- fixed platform-item misalignment while scrolling vertically
- double-click to open details of an item now works as expected
  (workaround in previous version was to press Escape and then double click)

0.9.3 (2017-01-16)

- When current "view" only has a defined startDate, set display range
  to one year after that.

0.9.2 (2016-12-12)

- fix issue that prevented tokens from being displayed when associated view
  does not have a defined time period (e.g., the "Public" view).
- turn the "View: _view-name_" label into an actual button (same effect as
  Refresh) so it can be clicked to reflect any update in the selected view
  (Note that the view selection is done in the Situational Awareness tab.)
- include link to this ChangeLog page in the version label.

0.9.1 (2016-11-22)

- More usability improvements:
    - Now a vertical scroll bar is enabled on the timeline so, along
      with a maximum height, it does not take too much vertical space
      thus allowing to also interact with the map more easily.
    - When mouse is hovered on a geometry on the map, the corresponding
      timeline item is highlighted much more prominently.
      Also, the item is now focused at the center of the timeline.
    - Because of the new vertical scroll functionality, now the Ctrl
      key needs to be pressed while scrolling with mouse wheel
      or two fingers to zoom in/out the timeline.
      (But "pinch with two fingers" can also still be used for this.)

- Now using the ODSS View for the default period of time to be displayed.
  (The interim "periods" handling was removed.)


0.8.3: (2016-09-21)

- Usability improvements:
    - Now a click on the timeline widget is required to enable the mouse/trackpad handling for
      dragging, zooming in/out, etc. This helps with general scrolling of the main page especially
      when several platforms are selected in the timeline, so, for example, it's easier to focus the map
      on the lower part of the page.
      Hit the Esc key, or click elsewhere, to disable the mouse/trackpad handling in the timeline widget.
    - Additionally, mouse/trackpad dragging events on the platform section (left panel in the
      timeline) do not trigger zoom in/out handling anymore.

- Various internal changes (including a library update)

0.8.2: (2015-04-05)

- For the copy-and-add functionality, 'C' (uppercase) now also copies the geometry associated
  with the selected token, while 'c' (lowercase) does not.
- Many internal adjustments (library updates; simplified build process, ...)

0.8.1: (2015-09-16)

- initial implementation of time calculations for distances:
    - time is shown along with the distance for both existing line-strings (on mouse-over) and while
      drawing new ones for a token. The speed is taken from an interim hard-coded platform-speed association
    - time is also shown by the measureTool while using the distance option. A new 'speed' field
      in the UI allows the user to indicate the associated speed prior to start drawing the line-string.
      The field can be left empty or with 0 value to disable.
        ![](img/2015-09-16_1128.png)


0.8.0: (2015-06-03)

- initial version of measure tool
- zoom in/out and panning with key strokes on the map
- include polygon-area/linestring-length in geometry coordinate table dialog
- include lineString length / polygon area / point location information in tooltip over geometry component

0.7.4: (2015-05-20)

- ability to view and directly edit the coordinates of the geometries (in "view", "move" or "modify" mode,
  double-click the geometry component to be viewed or edited)
- filter input added to the platform selection dialog so it's easier to find entries in the long list

0.7.3:

- several usability improvements: better token tooltips; added tooltips on geometries;
  token/geometry selection can now be done on the map itself; bidirectional visual sync between timetime and map.
- escape key clears token selection
- map resized according to window size, with a minimum height (500px)
- reduce timeline item padding, mainly to save vertical estate

0.7.1:

- geometry can now be defined for brand new item in the timeline
- removal of token in timeline also removes associated geometry
- geometry modification now also triggers a change of the "modified" status of token for purposes of saving

0.7.0: initial geometry editing prototype

0.5.0:

- improved UI for periods; buttons reorganized in general interface; mechanisms to mark a token as of
  type "deployment" or "mission" (not yet documented here).
- "copy-and-add" token"
